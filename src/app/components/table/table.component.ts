import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  tableTitles: Array<any> = ["Payee", "Child", "3 Weeks", "2 Weeks", "1 Weeks", "Current", "Pending Amount", "Over Payment", "Pending Creditnote", "Total Pending Amount"]
  tableData:Array<any> = [
    {
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },
    {
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },
    {
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },{
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },{
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },{
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },
    
    {
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },
    {
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },
    {
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },{
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },{
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },{
      payee:"john Doe",
      child:"Berlin",
      threeWeeks:4098,
      twoWeeks:4028,
      weeks:4098,
      current:5361,
      pendingAmount:4098,
      overPayment:5362,
      pendingCreditnote:5361,
      totalPendingAmount:5361

    },
    
    
  ]
  totalThreeWeeks:number = 0
  twoWeeks:number = 0
  weeks:number = 0
  current:number = 0
  pendingAmount:number = 0
  overPayment:number = 0
  pendingCreditnote:number = 0
  total:number = 0

  constructor() { }

  ngOnInit(): void {
    for(let table = 0; table < this.tableData.length; table++){
        this.totalThreeWeeks = this.totalThreeWeeks + this.tableData[table].threeWeeks 
        this.twoWeeks = this.twoWeeks + this.tableData[table].twoWeeks
        this.weeks = this.weeks + this.tableData[table].weeks
        this.current = this.current + this.tableData[table].current
        this.pendingAmount = this.pendingAmount + this.tableData[table].pendingAmount
        this.overPayment = this.pendingAmount + this.tableData[table].overPayment
        this.pendingCreditnote = this.pendingAmount + this.tableData[table].pendingCreditnote
        this.total = this.total + this.tableData[table].totalPendingAmount
    }

  }

}
