/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      colors: {
        "header": "#E3F2F1",
        "border":"#ACCCD0",
        "sidebar":"#009688",
        "mainBody":"#F0F7F6",
        "filter":"#F6F8FB",
        "filterText":"#232323",
        "filterLabel":"#111827",
        "iconColor":"#EAECF7",
        "filterBorder":"#D8D9DF",
        "activeText":"#5DAED7",
        "paginationColor":"#A8ABAE"
      },
      maxHeight:{
        "screen/4": "40vh",
      },
      minHeight:{
        "screen/4": "45vh",
      },
      
      
    },
  },
  plugins: [],
}
