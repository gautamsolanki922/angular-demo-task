import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  active:boolean = false

  constructor() { }

  ngOnInit(): void {
  }

   onFilterOpen(isFilter:boolean) {
    this.active = isFilter

  }

}
