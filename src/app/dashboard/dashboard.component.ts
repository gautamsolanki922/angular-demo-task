import { Component, OnInit } from '@angular/core';
import { DataService } from '../dataservice.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  activeProjectIndex: number = 5
  isFilter:boolean = false

  public activeProject(index: number): void {
    this.activeProjectIndex = index;
  }
  tabs: Array<any> = ["Transaction List", "Invoices", "Payments", "Credit Notes", "Bond Payments", "Age Debtors"]
  constructor(private dataService : DataService) {

  }

  ngOnInit(): void {
    this.dataService.setTitle("Dashboard")
    this.dataService.showAlert("success","success","Welcome Dashboard")

  }

  onTabs(index:any){
    this.activeProjectIndex = index
  }

  openFilter(isFilter:boolean){
    this.isFilter = isFilter
  }
  closeFilter(isFilter:boolean){
    this.isFilter = isFilter

  }

}
