import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/dataservice.service';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent implements OnInit {

  isSidebar:any


  constructor(private dataService:DataService) {
   
   }

  ngOnInit(): void {

    this.dataService.isSidebar.subscribe((message) => {
      this.isSidebar = message
    });
  }

}
