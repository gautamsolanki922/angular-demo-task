import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Title } from '@angular/platform-browser';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private titleService: Title, private toastr: ToastrService) {}

  setTitle(title: string = 'Home', main: string = '| Juniorlogs ') {
    this.titleService.setTitle(`${title} ${main}`);
  }

  isSidebar = new Subject<any>();

  

  showAlert(type: string, title: string, message: string) {
    if (type == 'success') {
      this.toastr.success(message, '');
    } else if (type == 'error') {
      this.toastr.error(message, title);
    }
  }
}
