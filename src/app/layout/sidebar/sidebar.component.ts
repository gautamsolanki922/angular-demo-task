import { Component, OnInit,Input } from '@angular/core';
import { DataService } from 'src/app/dataservice.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
 
  @Input() isSidebar:any;


  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  }

  isCloseSidebar(isClose:any){
    this.dataService.isSidebar.next(isClose);

  }


}
