import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/dataservice.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit(): void {
  }

  openSidebar(value:boolean){
    this.dataService.isSidebar.next(value);
   
  }

}
